# Opinion polling LOESS graph
Download:

- [R](https://ftp.fau.de/cran/)
- [RStudio](https://rstudio.com/products/rstudio/download/#download)
- "ggplot.R" and "DE.csv" from here to your working directory

Create an R project in the working directory.

In RStudio, run these commands to install packages:

```
> install.packages("tidyverse")
> install.packages("backports")
> install.packages("anytime")
> install.packages("svglite")
```

Use "DE.csv" to add polling data. Then run "ggplot.R" to create graph, which will be saved as "polls.svg".

Recommendations:
- For parties which have fewer polling data (i.e. parties founded during the legislature), a separate higher "span" should be used. If the same value is applied as for all other parties, the curve will look much more volatile.
- If your curve starts having straight segments and edges, you should increase the "nnum" value.
- I recommend using a combination of Notepad++ (Search/Replace) and Excel (remove columns) to edit the data from Wikipedia tables and transforming them into the right format.
- You can find the correct party colors here: https://en.wikipedia.org/wiki/Module:Political_party#Data_pages
- When uploading to Commons, I recommend linking to this page in the description so that other users can find this template as well.
- If you have any questions or proposals, contact me on my Wikipedia talk page (https://en.wikipedia.org/wiki/User_talk:Gbuvn).
- Also feel free to propose improvements to the code, especially if you can find a setup, which makes the inclusion of additional parties easier